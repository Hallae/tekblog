using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Techblog.Areas.Identity.Data;
namespace Techblog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
                        var connectionString = builder.Configuration.GetConnectionString("DBContextblogConnection") ?? throw new InvalidOperationException("Connection string 'DBContextblogConnection' not found.");

                                    builder.Services.AddDbContext<DBContextblog>(options =>
                options.UseSqlServer(connectionString));

                                                builder.Services.AddDefaultIdentity<TechblogUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<DBContextblog>();

            // Add services to the container.
            builder.Services.AddControllersWithViews();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
                        app.UseAuthentication();;

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");


            //this is responsible for making the register page load
            app.MapRazorPages();
            app.Run();
        }
    }
}