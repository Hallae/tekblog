﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Build.Framework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Techblog.Areas.Identity.Data;

namespace Techblog.Areas.Identity.Data;

public class DBContextblog : IdentityDbContext<TechblogUser>
{
    public DBContextblog(DbContextOptions<DBContextblog> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.

        // Add your customizations after calling base.OnModelCreating(builder);

        builder.ApplyConfiguration(new ApplicationUserEntityConfiguration()); // this takes the class below
    }

    public class ApplicationUserEntityConfiguration : IEntityTypeConfiguration<TechblogUser>
    {
        public void Configure(EntityTypeBuilder<TechblogUser> builder)
        {
            builder.Property(x => x.FirstName).HasMaxLength(100);
            builder.Property(x => x.LastName).HasMaxLength(100);

        }
    }



}
